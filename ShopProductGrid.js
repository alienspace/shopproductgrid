import React, { Component} from 'react';
import { nextStorage } from './../../../util/nextStorage';
import {
  Image, Button,
} from 'react-bootstrap';
import { browserHistory, Link } from 'react-router';
// import slug from 'slug';

/** COMPONENTS CUSTOM **/
// import { APP_CONFIG } from './../../boot.config';
import { TOOLS } from './../../../util/tools.global';
export class ShopProductGrid extends Component{
  constructor(props){
    super(props);
    this.state = {
      productsPhotos: []
    }

    this.addItemCart = this.addItemCart.bind(this);
    this.getProducts = this.getProducts.bind(this);
  }

  getProducts(){
    return this.props.products;
  };//getProducts();

  addItemCart(e){
    e.preventDefault();
    // console.log('dataset click ',e.target.parentElement.parentElement.parentElement.children[0].dataset._id);
    let nextDash = nextStorage.getJSON('nextDash');

    let
      productInner = e.target.parentElement.parentElement.parentElement.children[0],
      productID = productInner.dataset._id,
      productPhotoPrimary = productInner.children[0].children[0].children[0].src,
      productTitle = productInner.children[2].innerHTML,
      productSlug = e.target.dataset.slug,
      // eslint-disable-next-line
      productQty = productInner.children[3].innerHTML,
      productPrice = productInner.children[6].innerHTML;

      /*
      console.log('productInner -> ', productInner);
      console.log('productID -> ', productID);
      console.log('productPhotoPrimary -> ', productPhotoPrimary);
      console.log('productTitle -> ', productTitle);
      console.log('productSlug -> ', productSlug);
      console.log('productQty -> ', productQty);
      console.log('productPrice -> ', productPrice);
      */

    let existItem=false;
    // eslint-disable-next-line
    nextDash.CART_productsList.map((product) => {
      if(product.productID === productID){
        product.productPhotoPrimary = productPhotoPrimary;
        // eslint-disable-next-line
        product.productQty = parseInt(product.productQty) + parseInt(productQty);
        product.productSlug = product.slug;
        existItem = true;
      }
    });

    if(!existItem){
      nextDash.CART_productsList.push({
        productID:productID,
        productPhotoPrimary: productPhotoPrimary,
        productTitle:productTitle,
        productSlug: productSlug,
        productQty:productQty,
        productPrice:productPrice
      });
    }

    nextDash.CART_idOrder = nextDash.CART_idOrder;
    nextDash.CART_productsList = nextDash.CART_productsList;
    nextStorage.set('nextDash',nextDash);
    // console.log('nextStorage in ProductGrid -> ',nextStorage.getJSON('nextDash'));
    browserHistory.push({
      pathname: '/',
      query: {"action":'updateCart'}
    });
  }//addItemCart(e);

  renderGrid(){
    // console.log('LayoutComponent:',this.props.layoutComponent);
    let allProducts = this.getProducts();
    let itemsPerLine = 4,
      widthItem = (12 / itemsPerLine),
      classDefault = 'productItem col-md-' + widthItem;
    if(allProducts.length < 1){
      return (<li className={classDefault}>Loading...</li>);
    }
    return allProducts.map((product) => {
      // console.log('allProducts - product -> ' , product);
      return (
        <li key={product.productID} className={classDefault}>
          <div className="inner" data-_id={product.productID}>
            <Link to={'/product/'+product.slug} alt={product.title}>
              {product.photoPrimary ?
                <figure><Image src={product.photoPrimary} className="img-responsive" /></figure>
              :
                <figure><Image src={'assets/images/facebug.png'} className="img-responsive" /></figure>
              }
            </Link>
            <div className={'hover_shortcuts'}>
              <Link to={'/'} className={'fa fa-heart'} title={'Adicionar à lista de desejos'}></Link>
              <Link to={'/'} className={'fa fa-shopping-cart'} title={'Adicionar ao carrinho'}></Link>
            </div>
            <h3><Link to={'/product/'+product.slug} title={product.title}>{product.title}</Link></h3>
            <span className={'hidden'}>1</span>
            <i className={'label label-xs label-warning'}>{product.category}</i>
            <p>{TOOLS.stripAllTags(product.shortDescription)}</p>
            <strong>{'R$ ' + product.price}</strong>
            <Button
              ref={'comprar'}
              onClick={this.addItemCart}
              data-_id={product.productID}
              data-title={product.title}
              data-slug={product.slug}
              data-qty={1}
              data-price={product.price}
              className={'btn btn-md btn-primary'}>
              <span>COMPRAR</span>
            </Button>
          </div>
        </li>
      );
    });
  };//renderGrid();

  render() {
    const
      ShopTitletGrid = this.props.layoutComponent.titleGrid ? <h3>{this.props.layoutComponent.titleGrid}</h3> : '';
      let classDefault = 'col-md-12';
    if(this.props.className)
      classDefault = classDefault + ' ' + this.props.className;

    return (
      <div id="ShopProductGrid" className={classDefault}>
        <ol className={'breadcrumb'}>
          <li><a href="#">Home</a></li>
          <li><a href="#">Library</a></li>
          <li className={'active'}>Data</li>
        </ol>
        {ShopTitletGrid}
        <ul className="productList col-md-12">
          {this.renderGrid()}
        </ul>
      </div>
    );
  }

}
